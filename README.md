# Final Project

This is the Repository for group 18 in HUGB.

## Students / Authors

    - Rikharður Aron Eiríksson : @Rikki_
    - Óðinn Karl Skúlason : @onni21
    - Marteinn Hjálmarsson : @marteinnh
    - Gunnhildur Ásgeirsdóttir : @gunnhildur22
    - Halldór Smári Karlsson : @HallDori1
    - Rannveig Birta Sigurgeirsdóttir : @rannveig.sigurgeirs
    - Sara Atladóttir : @saraatlala

# Project Setup and Run Guide

      **Introduction:**

This guide will help you set up and run your project in Visual Studio Code.

1. **Open the Project in Visual Studio Code:**

   - Launch Visual Studio Code.
   - Open your project folder using File > Open Folder.

2. **Navigate to the Project Directory:**

   - Open the integrated terminal in Visual Studio Code.
   - Change directory to the 'src' folder:
     ```
     cd src
     ```

3. **Install gRPC (If not installed already):**

   - If gRPC is not installed, run the following commands in the terminal:
     ```
     python -m pip install grpcio
     python -m pip install grpcio-tools
     ```

4. **Start the gRPC Server:**

   - Run the following command in the terminal to start the server:
     ```
     python grpcserver.py
     ```
   - Confirm that the server is running with the message:
     ```
     Starting server. Listening on port 50051...
     ```

5. **Run the Main Python File:**
   - In the Visual Studio Code explorer, locate your main Python file (usually located in the 'src' directory).
   - Run the Python file: main.py.

# Running tests

**Run the test coverage:**

- If Coverage.py is not installed, run the following command in the terminal:
  ```
  python -m pip install coverage
  ```
- Run the following command in the terminal to run the tests:
  ```
   coverage run -m unittest discover
  ```

**Note:** Ensure that you have Python installed on your system before executing the commands.

#### For additional support or troubleshooting, contact the scrum master or project maintainers.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/HUGB_23G18/final_project.git
git branch -M main
git push -uf origin main
```

# Layers of the program:

- The UI layer is responsible for managing the user interface of the program. It handles the user's inputs and displays information to the user.
- The Logic layer processes and manages data from the UI layer. It handles the program's functionality.
- The Data layer is responsible for interacting with the databases. It performs tasks like reading, and writing data from database and manages data connections.
- The Utils layer contains utility functions and tools that can be used across the program, such as error messages and clear terminal.
- The model layer contains classes used to handle data all around the program.
