import os  # used for calling the system, like clearing the terminal
import platform

def clear_terminal():
    '''
    Clears the terminal screen based on the operating system.
    This function checks the operating system and clears the terminal screen accordingly. 
    If the operating system is not recognized, it prints a message indicating that the terminal cannot be cleared.
    '''
    system = platform.system()
    if system == 'Linux' or system == 'Darwin':  # Unix/Linux/Mac OS
        os.system('clear')
    elif system == 'Windows':   # Windows
        os.system('cls')
    else:
        # If the operating system is not recognized, print a message
        print("Unsupported operating system. Cannot clear the terminal.")
