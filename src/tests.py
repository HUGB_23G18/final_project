import unittest
from unittest.mock import Mock, patch

from data.AccountData import AccountData
from data.QuizData import QuizData
from grpcService.client import run
from logic.countryLogic import CountryLogic
from logic.quizLogic import QuizLogic
from model.createAccount import Account
from model.quiz import Quiz
from ui.createAccount import CreateAccount
from ui.menu import Menu

LINE = '------------------------------------------'
class TestQuizLogic(unittest.TestCase):
    def setUp(self):
        self.quiz_logic = QuizLogic()

    def test_get_valid_budget_valid(self):
        valid_budget = "100"
        result = self.quiz_logic.get_valid_budget(valid_budget)
        self.assertTrue(result)

    def test_get_valid_budget_empty(self):
        empty_budget = ""
        result = self.quiz_logic.get_valid_budget(empty_budget)
        self.assertFalse(result)

    def test_get_valid_budget_negative(self):
        negative_budget = "-50"
        result = self.quiz_logic.get_valid_budget(negative_budget)
        self.assertFalse(result)

    def test_get_valid_budget_non_integer(self):
        non_integer_budget = "abc"
        result = self.quiz_logic.get_valid_budget(non_integer_budget)
        self.assertFalse(result)

    def test_get_valid_fancy_valid(self):
        valid_fancy = "3"
        result = self.quiz_logic.get_valid_fancy(valid_fancy)
        self.assertTrue(result)

    def test_get_valid_fancy_empty(self):
        empty_fancy = ""
        result = self.quiz_logic.get_valid_fancy(empty_fancy)
        self.assertFalse(result)

    def test_get_valid_fancy_out_of_range(self):
        out_of_range_fancy = "6"
        result = self.quiz_logic.get_valid_fancy(out_of_range_fancy)
        self.assertFalse(result)

    def test_get_valid_fancy_non_integer(self):
        non_integer_fancy = "abc"
        result = self.quiz_logic.get_valid_fancy(non_integer_fancy)
        self.assertFalse(result)

    def test_get_valid_couple_valid_numeric(self):
        valid_couple = "1"
        result = self.quiz_logic.get_valid_couple(valid_couple)
        self.assertTrue(result)

    def test_get_valid_couple_valid_string_yes(self):
        valid_couple = "yes"
        result = self.quiz_logic.get_valid_couple(valid_couple)
        self.assertTrue(result)

    def test_get_valid_couple_valid_string_y(self):
        valid_couple = "y"
        result = self.quiz_logic.get_valid_couple(valid_couple)
        self.assertTrue(result)

    def test_get_valid_couple_empty(self):
        empty_couple = ""
        result = self.quiz_logic.get_valid_couple(empty_couple)
        self.assertIsNone(result)

    def test_get_valid_couple_invalid_numeric(self):
        invalid_couple = "3"
        result = self.quiz_logic.get_valid_couple(invalid_couple)
        self.assertIsNone(result)

    def test_get_valid_couple_invalid_string(self):
        invalid_couple = "invalid"
        result = self.quiz_logic.get_valid_couple(invalid_couple)
        self.assertIsNone(result)
    
    def test_get_valid_family_valid_numeric_yes(self):
        valid_family = "1"
        result = self.quiz_logic.get_valid_family(valid_family)
        self.assertTrue(result)

    def test_get_valid_family_valid_string_yes(self):
        valid_family = "yes"
        result = self.quiz_logic.get_valid_family(valid_family)
        self.assertTrue(result)

    def test_get_valid_family_valid_string_y(self):
        valid_family = "y"
        result = self.quiz_logic.get_valid_family(valid_family)
        self.assertTrue(result)

    def test_get_valid_family_empty(self):
        empty_family = ""
        result = self.quiz_logic.get_valid_family(empty_family)
        self.assertIsNone(result)

    def test_get_valid_family_invalid_numeric(self):
        invalid_family = "3"
        result = self.quiz_logic.get_valid_family(invalid_family)
        self.assertIsNone(result)

    def test_get_valid_family_invalid_string(self):
        invalid_family = "invalid"
        result = self.quiz_logic.get_valid_family(invalid_family)
        self.assertIsNone(result)

    def test_get_valid_adventure_valid_numeric_yes(self):
        valid_adventure = "1"
        result = self.quiz_logic.get_valid_adventure(valid_adventure)
        self.assertTrue(result)

    def test_get_valid_adventure_valid_string_yes(self):
        valid_adventure = "yes"
        result = self.quiz_logic.get_valid_adventure(valid_adventure)
        self.assertTrue(result)


class TestQuizData(unittest.TestCase):
    def setUp(self):
        self.mock_quiz_data = Mock()
        self.quiz_data = QuizData()
        self.quiz_data.quiz_data = self.mock_quiz_data
        self.quiz_data.file = "database/QuizDB.csv"

    def test_get_quiz_by_userId_not_found(self):
        # Mock the data that will be returned by the CSV file
        mock_data = [{'userId': '1', 'budget': '100'}, {'userId': '2', 'budget': '200'}]
        self.mock_quiz_data.read_all_quizzes.return_value = mock_data

        quiz = self.quiz_data.get_quiz_by_userId('3')

        # Ensure that the get_quiz_by_userId function returns None when the user ID is not found
        self.assertIsNone(quiz)


class TestQuizModel(unittest.TestCase):
    def setUp(self):
        self.user_id = 123
        self.budget = '100'
        self.couple = 1
        self.family = 1
        self.adventure = 1
        self.isHot = 1

    def test_quiz_creation(self):
        quiz = Quiz(self.user_id, self.budget, self.couple, self.family, self.adventure, self.isHot)
        
        self.assertEqual(quiz.userId, self.user_id)
        self.assertEqual(quiz.budget, self.budget)
        self.assertEqual(quiz.couple, self.couple)
        self.assertEqual(quiz.family, self.family)
        self.assertEqual(quiz.adventure, self.adventure)
        self.assertEqual(quiz.isHot, self.isHot)


class TestAccountData(unittest.TestCase):
    def setUp(self):
        self.account_data = AccountData()
        self.account_data.file = "src/database/UserDB.csv"

    def test_authenticate_invalid(self):
        username = 'user1'
        password = 'invalid_pass'
        accounts = [
            Account('1', 'user1', 'pass1', 'user1@example.com'),
            Account('2', 'user2', 'pass2', 'user2@example.com')
        ]
        self.account_data.read_all_accounts = Mock(return_value=accounts)

        user_id = self.account_data.authenticate(username, password)

        self.assertIsNone(user_id)
    
    def test_authenticate_valid(self):
        username = 'sara'
        password = 'Blabla1'
        accounts = [
            Account('1', 'new_user1', 'Ponni2', 'onni@gmail.com'),
            Account('2', 'sara', 'Blabla1', 'sara@email.is3')
        ]
        self.account_data.read_all_accounts = Mock(return_value=accounts)

        user_id = self.account_data.authenticate(username, password)

        self.assertEqual(user_id, '2')

    def test_edit_user_info_username(self):
        user_info = {
            'id': '1',
            'username': 'new_user1',
            'password': 'Ponni2',
            'email': 'onni@gmail.com',
            '[countryId]': []
        }
        field_edit = 'username'
        new_val = 'new_user1'
        accounts = [Account(user_info['id'], user_info['username'], user_info['password'], user_info['email'], user_info['[countryId]'])]
        self.account_data.read_all_accounts = Mock(return_value=accounts)

        self.account_data.edit_user_info(user_info, field_edit, new_val)
        updated_accounts = self.account_data.read_all_accounts()
        print(updated_accounts[0].username)
        print("AND HEREERERREEREREER")
        print(new_val)

        self.assertEqual(updated_accounts[0].username, new_val)


    def test_get_user_info_user_id_found(self):
        user_info = {
            'id': '1',
            'username': 'user1',
            'password': 'pass1',
            'email': 'user1@example.com',
            '[countryId]': [1, 2, 3]
        }
        user_id = '2'
        accounts = [Account(user_info['id'], user_info['username'], user_info['password'], user_info['email'], user_info['[countryId]'])]
        self.account_data.read_all_accounts = Mock(return_value=accounts)

        returned_user_info, error_message = self.account_data.get_user_info(user_id)

        self.assertEqual(returned_user_info, {'id': '2', 'username': 'sara', 'password': 'Blabla1', 'email': 'sara@email.is3', '[countryId]': '[7631]'})
        self.assertIsNone(error_message)

    def test_get_user_info_from_username_found(self):
        username = 'new_user1'
        user_info = {
            'id': '1',
            'username': 'new_user1',
            'password': 'Ponni2',
            'email': 'onni@gmail.com',
            '[countryId]': []
        }
        accounts = [Account(user_info['id'], user_info['username'], user_info['password'], user_info['email'], user_info['[countryId]'])]
        self.account_data.read_all_accounts = Mock(return_value=accounts)

        returned_user_info, error_message = self.account_data.get_user_info_from_username(username)

        self.assertEqual(returned_user_info['username'], user_info['username'])
        self.assertIsNone(error_message)

    def test_get_user_info_from_username_not_found(self):
        username = 'user2'
        user_info = {
            'id': '1',
            'username': 'user1',
            'password': 'pass1',
            'email': 'user1@example.com',
            '[countryId]': [1, 2, 3]
        }
        accounts = [Account(user_info['id'], user_info['username'], user_info['password'], user_info['email'], user_info['[countryId]'])]
        self.account_data.read_all_accounts = Mock(return_value=accounts)

        returned_user_info, error_message = self.account_data.get_user_info_from_username(username)

        self.assertIsNone(returned_user_info)
        self.assertEqual(error_message, "No user with username 'user2' was found!")




class TestCreateAccount(unittest.TestCase):
    def setUp(self):
        # Create a mock instance of CreateAccount for testing
        self.create_account = CreateAccount(lastId=None)  # Pass appropriate arguments

    @patch('builtins.input', side_effect=['ValidUsername'])
    def test_create_username_valid(self, mock_input):
        username = self.create_account.create_username()
        self.assertEqual(username, 'ValidUsername')  # Check if the username is correctly returned

    @patch('builtins.input', side_effect=['InvalidUsername'])
    def test_create_username_invalid_repeated(self, mock_input):
        username = self.create_account.create_username()
        self.assertEqual(username, 'InvalidUsername')  # Check if the username is correctly returned

    @patch('builtins.input', side_effect=['ValidPassword123'])
    def test_create_password_valid(self, mock_input):
        flag = 0
        password = self.create_account.create_password(flag)
        self.assertEqual(password, 'ValidPassword123')  # Check if the password is correctly returned

    @patch('builtins.input', side_effect=['Invalid', 'ValidPassword123'])
    def test_create_password_invalid_then_valid(self, mock_input):
        flag = 0
        password = self.create_account.create_password(flag)
        self.assertEqual(password, 'ValidPassword123')  # Check if the password is correctly returned

    @patch('builtins.input', side_effect=['Invalid', 'ValidPassword123'])
    def test_create_password_invalid_then_valid(self, mock_input):
        flag = 0
        password = self.create_account.create_password(flag)
        self.assertEqual(password, 'ValidPassword123')  # Check if the password is correctly returned

    @patch('builtins.input', side_effect=['InvalidPassword', 'ValidPassword123'])
    def test_create_password_invalid_repeated(self, mock_input):
        flag = 0
        password = self.create_account.create_password(flag)
        self.assertEqual(password, 'ValidPassword123')  # Check if the password is correctly returned

    @patch('builtins.input', side_effect=['InvalidPassword', 'ValidPassword123'])
    def test_create_password_invalid_repeated_with_flag(self, mock_input):
        flag = -1
        password = self.create_account.create_password(flag)
        self.assertEqual(password, 'ValidPassword123')  # Check if the password is correctly returned

    @patch('builtins.input', side_effect=['ValidEmail@example.com'])
    def test_create_email_valid(self, mock_input):
        email = self.create_account.create_email()
        self.assertEqual(email, 'ValidEmail@example.com')  # Check if the email is correctly returned

    @patch('builtins.input', side_effect=['InvalidEmail', 'ValidEmail@example.com'])
    def test_create_email_invalid_then_valid(self, mock_input):
        email = self.create_account.create_email()
        self.assertEqual(email, 'ValidEmail@example.com')  # Check if the email is correctly returned

    @patch('builtins.input', side_effect=['InvalidEmail', 'ValidEmail@example.com'])
    def test_create_email_invalid_repeated(self, mock_input):
        email = self.create_account.create_email()
        self.assertEqual(email, 'ValidEmail@example.com')  # Check if the email is correctly returned

    def test_create_account(self):
        # You may write a test for create_account if applicable
        pass



class TestMenu(unittest.TestCase):
    def setUp(self):
        self.intro = "Welcome to the program"
        self.options = ["Option 1", "Option 2", "Option 3"]
        self.menu = Menu(self.intro, self.options)

    @patch('builtins.input', side_effect=['1'])
    def test_get_selection_valid(self, mock_input):
        expected_result = 0
        result = self.menu.get_selection()
        self.assertEqual(result, expected_result)

    @patch('builtins.input', side_effect=['B'])
    def test_get_selection_back(self, mock_input):
        expected_result = -1
        result = self.menu.get_selection()
        self.assertEqual(result, expected_result)

class TestCountryLogic(unittest.TestCase):
    def setUp(self):
        self.country_logic = CountryLogic()

    def test_get_country_by_id_existing_country(self):
        # Test when a valid country ID is provided.
        country_id = "1"
        result, error_message = self.country_logic.get_country_by_id(country_id)
        self.assertIsNotNone(result)
        self.assertIsNone(error_message)

    def test_get_country_by_id_non_existing_country(self):
        # Test when a non-existing country ID is provided.
        country_id = "999"
        result, error_message = self.country_logic.get_country_by_id(country_id)
        self.assertIsNone(result)
        self.assertIsNotNone(error_message)

    def test_get_all_countries(self):
        # Test when there are existing countries in the database.
        result, error_message = self.country_logic.get_all_countries()
        self.assertIsNotNone(result)
        self.assertIsNone(error_message)




if __name__ == '__main__':
    unittest.main()
