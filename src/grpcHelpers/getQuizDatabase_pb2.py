# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: proto/getQuizDatabase.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x1bproto/getQuizDatabase.proto\x12\x04quiz\"`\n\x0bQuizRequest\x12\x0e\n\x06userId\x18\x01 \x01(\x05\x12\x0e\n\x06\x62udget\x18\x02 \x01(\x05\x12\x0e\n\x06\x63ouple\x18\x03 \x01(\x08\x12\x0e\n\x06\x66\x61mily\x18\x04 \x01(\x08\x12\x11\n\tadventure\x18\x05 \x01(\x08\"\x1f\n\x0cQuizResponse\x12\x0f\n\x07message\x18\x01 \x01(\t2?\n\x0bQuizService\x12\x30\n\x07GetQuiz\x12\x11.quiz.QuizRequest\x1a\x12.quiz.QuizResponseb\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'proto.getQuizDatabase_pb2', _globals)
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _globals['_QUIZREQUEST']._serialized_start=37
  _globals['_QUIZREQUEST']._serialized_end=133
  _globals['_QUIZRESPONSE']._serialized_start=135
  _globals['_QUIZRESPONSE']._serialized_end=166
  _globals['_QUIZSERVICE']._serialized_start=168
  _globals['_QUIZSERVICE']._serialized_end=231
# @@protoc_insertion_point(module_scope)
