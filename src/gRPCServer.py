import grpc
from grpcHelpers.getUserID_pb2_grpc import add_UserServiceServicer_to_server
from grpcHelpers.getCountryDB_pb2_grpc import add_CountryServiceServicer_to_server
from grpcService.gRPCService import UserService, CountryService # Import your service implementation
from grpcService.client import run
from concurrent import futures

# Define the maximum concurrent RPCs the server can handle
MAX_WORKERS = 10

def serve():
    # Create a gRPC server
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=MAX_WORKERS))

    # Add the service to the server
    add_UserServiceServicer_to_server(UserService(), server)
    add_CountryServiceServicer_to_server(CountryService(), server)

    # Listen on port 50051
    server.add_insecure_port('[::]:50051')

    print('Starting server. Listening on port 50051...')
    server.start()
    run(1,1)

    # Keep the server running
    try:
        while True:
            pass
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()
    
