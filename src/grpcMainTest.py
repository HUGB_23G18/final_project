""" import grpc
from grpcHelpers import getCountryDB_pb2
from grpcHelpers.getCountryDB_pb2_grpc import CountryServiceStub
def countrie_list_to_dict():
    channel = grpc.insecure_channel('localhost:50051')
    stub = CountryServiceStub(channel)
    try:
        response = stub.GetCountryList(getCountryDB_pb2.EmptyMessage())
        
        all_countries = response.countries
            
    except grpc.RpcError as e:
        print("Error:", e)
    countries = {}
    for country_proto in all_countries:
        country_id = country_proto.countryId
        hotels_list = country_proto.hotels.split('/')
        country_info = {
            'Id': country_id,
            'Country': country_proto.country,
            'City': country_proto.city,
            'Description': country_proto.description,
            'IsHot': country_proto.isHot,
            'AvgPrice': country_proto.avgPrice,
            'Hotels': hotels_list,
            'Lat': country_proto.lat,
            'Lon': country_proto.lon,
        }
        # Add the country to the dictionary using its ID as the key
        countries[country_id] = country_info
    dict_check = not bool(countries)

    if dict_check:
        return (None, "There are no countries in the database.")
    else:
        return (countries, None)
    

if __name__ == '__main__':
    yes, no = countrie_list_to_dict()
    if yes is not None:
        print(yes)
 """