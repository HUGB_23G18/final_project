class Account:
    def __init__(self, userId, username, password, email, favCountries = []) -> None:
        '''
        Makes a model of quiz answers
        '''
        self.userId = userId
        self.username = username
        self.password = password
        self.email = email
        self.favoriteCountries = favCountries
    