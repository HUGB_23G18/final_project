class Country:
    def __init__(self, id, country, city, description, isHot : bool, avgprice, hotels) -> None:
        self.countrieId = id
        self.country = country
        self.city = city
        self.description = description
        self.isHot = isHot
        self.avgprice = avgprice
        self.hotels = hotels