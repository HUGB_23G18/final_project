
class Quiz:
    def __init__(self, userId, budget, couple, family, adventure, isHot) -> None:
        '''
        Makes a model of quiz answers
        '''
        self.userId = userId
        self.budget = budget
        self.couple = couple
        self.family = family
        self.adventure = adventure
        self.isHot = isHot
