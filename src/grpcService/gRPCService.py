import csv
import os

import grpc
from logic.countryLogic import CountryLogic

from logic.userLogic import UserLogic
from grpcHelpers.getUserID_pb2 import UserIDResponse
from grpcHelpers.getUserID_pb2_grpc import UserServiceServicer
from grpcHelpers import getCountryDB_pb2
from grpcHelpers.getCountryDB_pb2_grpc import CountryServiceStub, CountryService
from logic.quizLogic import QuizLogic
from grpcHelpers import getQuizDatabase_pb2
from grpcHelpers import getQuizDatabase_pb2_grpc

# Define the gRPC service
class UserService(UserServiceServicer):

    # Implementation of the GetUserID gRPC method
    def GetUserID(self, request, context):
        provided_username = request.username.lower()  # Get the username from the gRPC request
        user_logic = UserLogic()
        userData = user_logic.get_all_users()  # Read user data from the CSV file
        
        # Search for the user based on the provided username
        matching_user = None
        for user_id, user_info in userData.items():
            if user_info['username'].lower() == provided_username:
                matching_user = user_info
                break  # Stop the loop once a match is found

        if matching_user:
            return UserIDResponse(user_id=int(user_id))  # Convert user_id to int if necessary
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('')
            return UserIDResponse(user_id=0)  # Return 0 if user not found

class CountryService(CountryService):
    def get_country_list(self, request, context):
        # Create an instance of the CountryLogic class
        country_logic = CountryLogic()
        
        # Call the get_all_countries method of CountryLogic to fetch country data
        country_data, error_msg = country_logic.get_all_countries()  # Read country data from CSV

        # Create a response with the country data
        country_list_response = getCountryDB_pb2.CountryListResponse()

        # Iterate through the country data dictionary
        for country_id, data in country_data.items():
            if country_id == "countryId":
                continue  # Skip the entry if it's the header

            # Create a Country message for the current country
            country_proto = country_list_response.countries.add()
            country_proto.countryId = int(data['Id'])
            country_proto.country = data['Country']
            country_proto.city = data['City']
            country_proto.description = data['Description']
            country_proto.isHot = data['IsHot'].lower() == 'true'
            country_proto.avgPrice = float(data['AvgPrice'])
            country_proto.lat = float(data['Lat'])
            country_proto.lon = float(data['Lon'])

            # Split the hotels string into a list of hotels and join them with '/'
            hotels_list = data['Hotels']
            hotels_string = '/'.join(hotels_list)
            country_proto.hotels = hotels_string

        try:
            # Call the get_country_list gRPC method and return the response
            return country_list_response
        except grpc.RpcError as e:
            # Handle gRPC errors and return an empty response
            context.set_code(grpc.StatusCode.INTERNAL)
            context.set_details('Error: {}'.format(str(e)))
            return getCountryDB_pb2.CountryListResponse()

    def get_country_by_id(self, request, context):
        country_id = request.countryId

        country_logic = CountryLogic()

        country_data, error_msg = country_logic.get_country_by_id(country_id)

        if error_msg:
            context.set_code(grpc.StatusCode.Internal)
            context.set_details("Error: %s" % error_msg)
            return getCountryDB_pb2.Country()
        
        if country_data:
            country_proto = getCountryDB_pb2.Country()
            country_proto.countryId = int(country_data['Id'])
            country_proto.country = country_data['Country']
            country_proto.city = country_data['City']
            country_proto.description = country_data['Description']
            country_proto.isHot = country_data['IsHot'].lower() == 'true'
            country_proto.avgPrice = float(country_data['AvgPrice'])
            country_proto.lat = float(country_data['Lat'])
            country_proto.lon = float(country_data['Lon'])
            country_proto.hotels = '/'.join(country_data['Hotels'])

            return country_proto
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('Country with ID {} not found'.format(country_id))
            return getCountryDB_pb2.Country()

class QuizService(getQuizDatabase_pb2_grpc.QuizServiceServicer):
    def GetQuiz(self, request, context):
        logic = QuizLogic()
        budget_valid = logic.get_valid_budget(request.budget)
        couple_valid = logic.get_valid_couple(request.couple)
        family_valid = logic.get_valid_family(request.family)
        adventure_valid = logic.get_valid_adventure(request.adventure)

        if budget_valid and couple_valid is not None and family_valid is not None and adventure_valid is not None:
            # For simplicity, not saving the quiz in this example. 
            # You'd normally interact with your QuizData class here.
            return getQuizDatabase_pb2(message="Quiz answer saved")
        else:
            return getQuizDatabase_pb2.QuizResponse(message="Error in quiz data")