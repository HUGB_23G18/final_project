from logic.countryLogic import CountryLogic
from logic.userLogic import UserLogic

LINE = '------------------------------------------'

class FavoritesUI:
    """
    A class representing the user interface for displaying and managing favorite countries for a specific user.

    Attributes:
        user_id (int): The user ID for which the favorites are displayed.

    Methods:
        show_favorites(self): Display the list of favorite countries for the user and allow navigation to other options.
    """
    def __init__(self, user_id):
        self.user_id = user_id

    def show_favorites(self):
        """
        Display the list of favorite countries for the user and allow navigation to other options.

        The user's favorite countries are retrieved and displayed, allowing them to go back to the previous menu by
        pressing 'b'.
        """
        country_logic = CountryLogic()
        user_logic = UserLogic()
        while True:
            user_info = user_logic.get_user_info(self.user_id)
            favourites = user_info['[countryId]']
            favourites_list = []
            for element in favourites:
                favourites_list.append(element)
            countries, error_message = country_logic.get_all_countries()
            if error_message:
                print(error_message)
            else:
                print("Here are your favorites! ")
                print(LINE)
                for country_id, country_info in countries.items():
                    if country_id in favourites_list:
                        print(f"{country_id}: {country_info['Country']}, {country_info['Description']}")
            print(LINE)
            
            user_input = input("Press 'b' to go back: ").lower()
   
            if user_input == 'b':
                break 
