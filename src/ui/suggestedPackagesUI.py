
from logic.quizLogic import QuizLogic
from ui.booking import BookATrip


class SuggestedPackages():
    """
    A class for managing and displaying suggested travel packages based on a user's quiz answers.

    Attributes:
        userId (int): The user ID for which suggested packages are displayed.
        quiz (Quiz): An instance of the Quiz class containing user's quiz answers.

    Methods:
        __init__(self, userId, quiz): Initialize a SuggestedPackages object with user ID and quiz.
        input_prompt(self): Display and handle user input for selecting and booking suggested packages.
        print_package_details(self, package, counter): Display details of a travel package.
        get_suggested_packages(self): Get suggested travel packages based on user's quiz answers.
        save_selected_package(self, package_id): Save the user's selected package to the database.
        get_all_packages(self): Retrieve all available travel packages from the database.
    """
    def __init__(self, userId, quiz) -> None:
        self.userId = userId
        self.quiz = quiz

    def input_prompt(self):
        """
        Display and handle user input for selecting and booking suggested packages.

        This method presents suggested travel packages to the user, allows them to select and book a package, and
        provides options to change the package or go back.

        Returns:
            str: User input or command ("b" for back).
        """
        packages = self.get_suggested_packages()
        if len(packages) == 0:
            print("No packages found that match your criteria.")
            print("Reason: budget is too low")
            return "b"
        else:
            print("Suggested Packages:")
            counter = 1
            for package in packages:
                self.print_package_details(package, counter)
                counter += 1
        print("")
        print("Select a package by entering its ID or c to change to another package\nEnter b to go back. ")
        input_package = input("Package ID or c to change: ")
        selected_package = None
        while selected_package == None:
            try:
                input_package = int(input_package)
                selected_package = packages[int(input_package)-1]
            except ValueError:
                if input_package == "b":
                    return "b"
                if input_package == "c":
                    packages = self.get_all_packages()
                    print("All Packages:")
                    counter = 1
                    for package in packages:
                        self.print_package_details(package, counter)
                        counter += 1
                    print("")
                    print(
                        "Select a package by entering its ID. \nEnter b to go back. ")
                    input_package = input("Package ID: ")
        ui = BookATrip(self.userId, selected_package)
        ui.input_prompt()

        # self.save_selected_package(selected_package)

    def print_package_details(self, package, counter):
        """
        Display details of a travel package.

        Args:
            package (dict): A dictionary containing package information.
            counter (int): The package counter (used for display).

        Returns:
            None
        """
        # Add or remove details based on what is available in your package data
        details = f"Package {counter}:"

        if 'country' in package:
            details += f", Country: {package['country']}"
        if 'city' in package:
            details += f", City: {package['city']}"
        if 'description' in package:
            details += f", Description: {package['description']}"
        if 'AvgPrice' in package:
            details += f", Average Price: {package['AvgPrice']}"
        print("")
        print(details)

    def get_suggested_packages(self):
        '''
        This function gets suggested packages from the database based on the user's quiz answers.
        '''
        quiz_logic = QuizLogic()
        suggested_packages = quiz_logic.get_suggested_packages(self.quiz)
        return suggested_packages

    def save_selected_package(self, package_id):
        '''
        This function saves the user's selected package to the database.
        '''
        quiz_logic = QuizLogic()
        quiz_logic.save_selected_package(self.userId, package_id)

    def get_all_packages(self):
        """
        Retrieve all available travel packages from the database.

        Returns:
            list: A list of all available travel packages.
        """
        quiz_logic = QuizLogic()
        packages = quiz_logic.get_all_packages()
        return packages
