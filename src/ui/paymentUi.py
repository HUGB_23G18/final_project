import re

from ui.menu import Menu


class PaymentInformation:
    """
    A class for managing payment information, including creating, displaying, and processing payments.

    Attributes:
        newPaymentId (int): The ID of the newly created payment.

    Methods:
        __init__(self, lastPaymentId): Initialize a PaymentInformation object.
        create_payment(self): Create a new payment with user input.
        display_payment(self): Display payment information.
        process_payment(self): Process a payment.
    """
    def __init__(self, lastPaymentId):
        if lastPaymentId is None:
            self.newPaymentId = 1
        else:
            self.newPaymentId = int(lastPaymentId) + 1

    def create_payment(self):
        """
        Create a new payment with user input.

        Returns:
            int: The ID of the newly created payment.
        """
        while True:
            print("Payment Information:")
            card_number = input("Card Number: ")
            if not re.match(r'^\d{16}$', card_number):
                print("Invalid card number format. Please enter a 16-digit number.")
                continue

            card_holder = input("Card Holder Name: ")
            # if not re.match(r'^[A-Za-z\s]+$', card_holder):
            #     print("Invalid card holder name. Please enter a valid name.")
            #     continue

            expiration_date = input("Expiration Date (MM/YY): ")
            if not re.match(r'^(0[1-9]|1[0-2])\/\d{2}$', expiration_date):
                print("Invalid expiration date format. Please enter as MM/YY.")
                continue
            print("\nPayment Information Saved Successfully.")
            return self.newPaymentId
