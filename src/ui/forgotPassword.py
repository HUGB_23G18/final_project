from logic.userLogic import UserLogic

class ForgotPasswordUI:
    '''
    This class provides a user interface for resetting a forgotten password. 
    It prompts the user to enter their email, sends a verification code, and guides them through the process of changing their password.
    '''
    def __init__(self) -> None:
        pass

    def input_prompt(self):
        '''
        This function prompts the user to enter their email, validates the email and assists in creating a new password. 
        If the password reset is successful, it returns the user's ID.
        '''
        user_logic = UserLogic()
        email = input("Please enter your email: ")
        random_number = user_logic.validate_email(email)
        while random_number == None:
            print("Email not found")
            email = input("Please enter your email: ")
            random_number = user_logic.validate_email(email)
        input_random_number = input(f"Please enter the code sent to {email}: ")
        while input_random_number != random_number:
            print("Code is incorrect")
            input_random_number = input(
                f"Please enter the code sent to {email}: ")

        new_password = input("Please enter your new password: ")
        confirm_password = input("Please confirm your new password: ")
        while new_password != confirm_password:
            print("Passwords do not match")
            new_password = input("Please enter your new password: ")
            confirm_password = input("Please confirm your new password: ")
        if new_password == confirm_password:
            userId = user_logic.save_new_password(email, new_password)
            print("Password changed successfully")
            return userId
