from ui.createAccount import CreateAccount
from ui.forgotPassword import ForgotPasswordUI
from ui.logIn import LoginUI
from ui.menu import Menu
from data.DataUtils import getting_new_id_from_CSV

class Unauthoriced_UI:
    def __init__(self) -> None:
        pass

    def input_prompt(self):
        '''
        Prints a menu for the user to choose from
        If the user chooses "Log in", it calls the log_in function
        If the user chooses "Create an account", it calls the create_account function
        '''
        while True:
            intro = "Welcome to Travel8tor \nPlease choose an option."
            options = ["Log in", "Create an account", "Forgot password"]
            menu = Menu(intro, options)
            selection = menu.get_selection()
            if selection < 0:
                return "q"
            if options[selection] == "Log in":
                ui = LoginUI()
                userId = ui.log_in()
                loggedIn = True
                return userId
            if options[selection] == "Create an account":
                create_account = CreateAccount(
                    getting_new_id_from_CSV("UserDB.csv"))
                userId = create_account.create_account()
                loggedIn = True
                return userId
            if options[selection] == "Forgot password":
                forgot_password = ForgotPasswordUI()
                userId = forgot_password.input_prompt()
                return userId
