from logic.BookingLogic import BookingLogic

LINE = '------------------------------------------'

class BookedTripsUI:
    '''
    This class provides a user interface for displaying past bookings for a given user.
    '''
    def __init__(self, user_id):
        self.user_id = user_id

    def show_past_bookings(self):
        '''
        This function prints out all passed bookings for a specific userID and if there are no bookings it prints out a 
        "error message". 
        '''
        booking_logic = BookingLogic()

        while True:
            bookings = booking_logic.get_all_bookings()
            if bookings is None:
                print("No bookings are in the database!")
                break

            print("Here are your past bookings: ")
            print(LINE)

            user_bookings = [booking_info for booking_info in bookings.values() if str(booking_info.get('userId', '')) == str(self.user_id)]
            if not user_bookings:
                print("No bookings found for your user ID.")
            else:
                for booking_info in user_bookings:
                    print(f"User ID: {booking_info['userId']}")
                    print(f"Country ID: {booking_info['countryId']}")
                    print(f"Date booked: {booking_info['Date booked']}")
                    print(f"Price: {booking_info['price']}")
                    print(LINE)

            user_input = input("Press 'b' to go back: ").lower()

            if user_input == 'b':
                break



