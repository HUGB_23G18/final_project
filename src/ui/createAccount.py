import grpc
from grpcHelpers.getUserID_pb2 import Credentials
from grpcHelpers.getUserID_pb2_grpc import UserServiceStub

import re
from utils.utilities import clear_terminal
from ui.menu import Menu
from data.AccountData import AccountData
server_address = 'localhost:50051'


class CreateAccount:
    '''
    This class provides functionalities for creating a new user account, including creating a valid username, password, and email address. 
    It interacts with a gRPC service to check username availability and enforces specific rules for password and email validation.
    '''

    def __init__(self, lastId):
        # Initialize the class with a new user ID based on the last user ID
        if lastId is None:
            self.newId = 1
        else:
            self.newId = int(lastId) + 1

    def create_username(self):
        '''
        This function guides the user to input a valid username, checking its length and availability.
        '''
        # Create a gRPC channel and stub for the UserService
        channel = grpc.insecure_channel(server_address)

        # Prepare the request with the provided username
        stub = UserServiceStub(channel)

        while True:
            user_input = input("Username: ")
            # Prepare the request
            request = Credentials(username=user_input)
            if len(user_input) > 20 or len(user_input) < 2:
                print("\nPlease try again and follow the instructions below\n\nUsername cannot be over 10 characters long nor shorter than 2\n\n")

            else:
                try:
                    # Call the gRPC method to check if the username is available
                    stub.GetUserID(request)
                    print("Username already in database, please try again\n")

                except grpc.RpcError as e:
                    print("WHAT!")
                    print(e.details())
                    return user_input  # Call the gRPC method to check if the username is available

    def create_password(self, flag):
        '''
        This function guides the user to input a valid password, enforcing specific criteria.
        '''
        while True:
            print("""
            - One uppercase letter
            - One lowercase letter
            - A number
            - Not longer than 20 characters
            - Not shorter than 6 characters
            """)
            password = input("Password: ")
            if (len(password) < 6 or len(password) > 20):
                flag = -1
            elif not re.search("[a-z]", password):
                flag = -1
            elif not re.search("[A-Z]", password):
                flag = -1
            elif not re.search("[0-9]", password):
                flag = -1
            elif re.search("\s", password):
                flag = -1
            else:
                flag = 0
                print("Valid Password")
                break
            if flag == -1:
                flag = 0
                clear_terminal()
                print("Not a Valid Password ")
        return password

    def create_email(self):
        '''
        This function guides the user to input and validate a valid email address.
        '''
        while True:
            email = input("Email: ")
            if not (re.search("@", email) and (email.endswith(".com") or email.endswith(".is"))):
                if not re.search("@", email):
                    print("\nThere is no @")
                if not (email.endswith(".com") or email.endswith(".is")):
                    print("\nYou forgot to add .com or .is at the end\n")
            else:
                return email

    def change_input(self, un, pw, em, change, flag):
        '''
        This function allows the user to change their account information
        '''

        while True:
            change = input(
                "What would you like to change (1 for username, 2 for password, 3 for email)? ")
            choices = change.split(',')  # Split the input by comma

            if not choices:
                print("No choices provided.")
                continue

            for choice in choices:
                if choice == '1':
                    un = self.create_username()
                elif choice == '2':
                    pw = self.create_password(flag)
                elif choice == '3':
                    em = ""
                    em = self.create_email()
                else:
                    print(f"Invalid choice: {choice}")

            print("\nUpdated account information:")
            print(f"1. Username: {un}")
            print(f"2. Password: {pw}")
            print(f"3. Email: {em}")

            more_changes = input(
                "Do you want to make more changes? (yes or no): ").lower()
            if more_changes != 'yes':
                break

        return un, pw, em

    def create_account(self):
        '''
        This function initializes variables to store user information and flags
        '''

        username = ""
        password = ""
        email = ""
        flag = 0
        change = "1"  # Default change option

        # Display a message to prompt the user to create an account
        print("""
        Create an account
        Please fill in the username, password, and email sections

        Username cannot be over 20 characters long nor shorter than 2
        """)

        # Prompt the user to enter a username
        username = self.create_username()

        # Clear the terminal for a clean display
        # clear_terminal()

        # Prompt the user to create a password
        print("""
        Next is creating a password for your account
        The password must have
        """)
        password = self.create_password(flag)  # Get a valid password

        # Clear the terminal for a clean display
        clear_terminal()

        # Prompt the user to input and validate an email address
        print("Next we will be assigning the email to your account")
        email = self.create_email()

        # Clear the terminal for a clean display
        clear_terminal()

        # Display an overview of the provided account information
        print("Valid email address\n")
        print("Here is your account information overview")
        print("Username: " + username + "\nPassword: " +
              password + "\nEmail: " + email)

        # Offer the option to the user to change account information
        options = ["Yes", "No"]
        change = Menu(
            "\nIs there something you would like to change?", options)
        selection = change.get_selection()

        # If the user chooses to make changes, allow them to update account information
        if selection == 0:
            username, password, email = self.change_input(
                username, password, email, change, flag)

        # Store the account information in a list
        account = [self.newId, username, password, email, []]
        add_account = AccountData()
        add_account.add_account(account)
        return self.newId

        # Example usage/hvernig þið notið:
        # create_account = create_account(getting_new_id_from_CSV("UserDB.csv"))
        # create_account.create_account()
