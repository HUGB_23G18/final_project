from logic.userLogic import UserLogic

class LoginUI:
    '''
    This class represents the ui for the login screen.
    '''
    def __init__(self) -> None:
        pass

    def log_in(self):
        '''
        This funciton gets a username and password from user. Checks if the username and password is correct. 
        If correct, returns the userId
        If incorrect, returns None
        '''
        user_logic = UserLogic()
        username = input("Please enter your username: ")
        password = input("Please enter your password: ")
        userId = user_logic.authenticate(username, password)
        while userId == None:
            print("Username or password incorrect")
            username = input("Please enter your username: ")
            password = input("Please enter your password: ")
            userId = user_logic.authenticate(username, password)
        print("Login successful")
        return userId
