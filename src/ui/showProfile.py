
from logic.userLogic import UserLogic
from ui.editAccount import ProfileEditor
from ui.menu import Menu

LINE = '------------------------------------------'

class ShowProfile:
    """
    A class for displaying and managing user profiles.

    Attributes:
        user_id (int): The user ID for which the profile is displayed.

    Methods:
        __init__(self, user_id): Initialize a ShowProfile object for a specific user.
        show_profile(self): Display the user's profile and provide options for editing it.
    """
    def __init__(self, user_id) -> None:
        self.user_id = user_id
    
    def show_profile(self):
        """
        Display the user's profile and provide options for editing it.

        This method retrieves the user's profile information and allows the user to edit their profile if desired.

        Returns:
            None
        """
        logic = UserLogic()
        while True:
            user_info = logic.get_user_info(self.user_id)
            print("Your profile:")
            print(LINE)
            intro = f"Profile Pic:\n..............................\n..............................\n..............***.............\n...........*********..........\n..........***********.........\n..........***********.........\n...........*********..........\n..............***,............\n........***************.......\n.....*********************....\n....***********************...\nUsername: {user_info['username']}\nEmail: {user_info['email']}\nPassword: {user_info['password']}"
            options = ["Edit profile"]
            menu = Menu(intro, options)
            selection = menu.get_selection()

            if selection < 0:
                break
            if selection == 0:
                profile_editor = ProfileEditor(self.user_id)
                selection = profile_editor.edit_menu()
                



