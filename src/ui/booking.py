
import time
from logic.BookingLogic import BookingLogic
from ui.paymentUi import PaymentInformation
from utils.utilities import clear_terminal


class BookATrip:
    '''
    This class represents a booking process for a travel package.
    '''
    def __init__(self, userId, package) -> None:
        self.userID = userId
        self.package = package

    def input_prompt(self):
        '''
        Prompt the user for confirmation and initiate the booking process.
        '''
        print("You have selected the following package:")
        print("")
        print(f"Country: {self.package['country']}")
        print(f"City: {self.package['city']}")
        print(f"Description: {self.package['description']}")
        print(f"Average Price: {self.package['AvgPrice']}")
        print("")
        print("Press b to go back or press c to confirm booking")
        user_input = input("b or c: ")
        if user_input == "b":
            return "b"
        if user_input == "c":
            ui = PaymentInformation(None)
            ui.create_payment()
            logic = BookingLogic()
            logic.book(
                self.userID, self.package['countryId'], self.package['AvgPrice'])
        print("")
        print("Thank you for booking! Have a nice trip :)")
        print("An confirmation email has been sent to your email address.")
        time.sleep(4)
        clear_terminal()

    def send_confirmation_email(self, user_id, booking_id, datetime):
        '''
        Simulates sending a confirmation email when client has booked a trip. 
        '''
        return "c"
