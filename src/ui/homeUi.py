from logic.userLogic import UserLogic
from ui.editAccount import ProfileEditor
from ui.favoritesUi import FavoritesUI
from ui.locationUi import LocationUI
from ui.menu import Menu
from ui.quiz import QuizUI
from ui.showProfile import ShowProfile
from ui.suggestedPackagesUI import SuggestedPackages
from ui.bookTrip import BookedTripsUI


class HomeUI:
    '''
    This class provides a user interface for the home menu, where users can select various options such as taking a travel quiz, 
    exploring destinations, viewing favorites, accessing their profile, or editing their profile. 
    It utilizes other UI files and logic components.
    '''

    def __init__(self, userId) -> None:
        self.userId = userId

    def input_prompt(self):
        '''
        This function prompts the user for what they want to select. 
        '''
        user_logic = UserLogic()
        user_info = user_logic.get_user_info(self.userId)
        while True:
            options = ["Take the travel quiz!", "See destinations",
                       "My favourites", "See profile", "Edit profile", "Past bookings"]
            menu = Menu(
                f"Welcome {user_info['username']}, Please choose an option.", options)
            selection = menu.get_selection()
            if selection < 0:
                return "q"
            if selection == 0:
                ui = QuizUI(self.userId)
                quiz = ui.get_new_quiz()
                ui = SuggestedPackages(self.userId, quiz)
                ui.input_prompt()
            if selection == 1:
                ui = LocationUI(self.userId)
                ui.show_destinations()
            if selection == 2:
                ui = FavoritesUI(self.userId)
                ui.show_favorites()
            if selection == 3:
                ui = ShowProfile(self.userId)
                ui.show_profile()
            if selection == 4:
                profile_editor = ProfileEditor(self.userId)
                profile_editor.edit_menu()
            if selection == 5:
                ui = BookedTripsUI(self.userId)
                ui.show_past_bookings()
            return selection
