import re

from logic.userLogic import UserLogic
from ui.menu import Menu
from utils.exceptions import NotFoundException

LINE = '------------------------------------------'

class ProfileEditor:
    '''
    This class provides a UI for editing a user's profile, including their username and password. 
    It follows certain rules for both the username and password format and handles the editing process.
    '''
    def __init__(self, UserID):
        self.UserID = UserID
        self.user_logic = UserLogic()

    def menu_output(self):
        '''
        This function guides the user through editing their profile by calling the 
        methods to edit the username and password. 
        It returns the new username and password.
        '''
        new_username = self.edit_username()
        new_password = self.edit_password()
        return new_username, new_password

    def edit_username(self):
        '''
        This function guides the user through the process of changing their username, following character length and uniqueness rules.
        '''
        while True:
            new_username = input("Enter new username: ")
            if len(new_username) < 2 | len(new_username) > 10:
                print("\nPlease try again and follow the instructions below:\n")
                print("Username cannot be over 10 characters long nor shorter than 2\n")
            elif self.user_logic.check_for_duplicate_username(new_username):
                print("\nThis username is already in use. Please try again.\n")
            else:
                try:
                    self.user_logic.edit_user_info(
                        self.UserID, 'username', new_username)
                except NotFoundException as e:
                    print(e)
                    continue
                print(LINE)
                print("Your username has been updated!")
                print(LINE)
                return new_username

    def edit_password(self):
        '''
        This function guides the user through the process of changing their password, following certain rules for password complexity.
        '''
        user_logic = UserLogic()
        while True:
            print("""
            - One uppercase letter
            - One lowercase letter
            - A number
            - Not longer than 15 characters
            - Not shorter than 6 characters
            """)
            new_password = input("Enter new password: ")
            if (
                6 <= len(new_password) <= 15
                and re.search("[a-z]", new_password)
                and re.search("[A-Z]", new_password)
                and re.search("[0-9]", new_password)
                and not re.search("\s", new_password)
            ):
                try:
                    self.user_logic.edit_user_info(
                        self.UserID, 'password', new_password)
                except NotFoundException as e:
                    print(e)
                    continue
                print(LINE)
                print("Valid Password, so your password has been updated!")
                print(LINE)
                return new_password
            else:
                print("\n")
                print("Invalid Password, please try again.")

    def edit_menu(self):
        '''
        This function provides a menu for the user to choose between editing the username and password. 
        '''
        while True:
            options = ["Edit username", "Edit password"]
            menu = Menu(
                f"Here you can edit your profile, Please choose an option.", options)
            selection = menu.get_selection()
            if selection == 0:
                self.edit_username()
            if selection == 1:
                self.edit_password()
            if selection < 0:
                return "q"
