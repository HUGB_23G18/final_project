from logic.quizLogic import QuizLogic
from model.quiz import Quiz


class QuizUI:
    '''
    This class provides a user interface for quizzes and budget input. It allows users to input their budget for a trip, validates the budget, and saves quiz answers. 
    It utilizes the QuizLogic class for managing quiz-related operations.
    '''

    def __init__(self, userId) -> None:
        self.userId = userId

    def get_budget(self):
        '''
        This function prompts the user for their trip budget and validate it using a function from QuizLogic.
        '''
        print("""
        What is your budget for the trip?
        """)
        budget = input("budget (ISK): ")

        logic = QuizLogic()
        valid_budget = logic.get_valid_budget(budget)
        while valid_budget == False:
            print("Invalid budget")
            budget = input("budget (ISK): ")
            valid_budget = logic.get_valid_budget(budget)
        return budget

    def get_couple(self):
        print("""
              Are you travelling with a partner?
              """)
        print("1. Yes")
        print("2. No")
        couple = input("Yes or No: ")
        logic = QuizLogic()
        couple = logic.get_valid_couple(couple)
        while couple == None:
            print("Invalid input, please enter Yes or No")
            couple = input("Yes or No: ")
            couple = logic.get_valid_couple(couple)
        return couple

    def get_family(self):
        print("""
              Are you travelling with family?
              """)
        print("1. Yes")
        print("2. No")
        family = input("Yes or No: ")
        logic = QuizLogic()
        family = logic.get_valid_family(family)
        while family == None:
            print("Invalid input, please enter Yes or No")
            family = input("Yes or No: ")
            family = logic.get_valid_family(family)
        return family

    def get_adventure(self):
        print("""
              Would you consider yourself and traveling partners as adventure seeking people?
              """)
        print("1. Yes")
        print("2. No")
        adventure = input("Yes or No: ")
        logic = QuizLogic()
        adventure = logic.get_valid_adventure(adventure)
        while adventure == None:
            print("Invalid input, please enter Yes or No")
            adventure = input("Yes or No: ")
            adventure = logic.get_valid_adventure(adventure)
        return adventure

    def get_isHot(self):
        print("""
              Do you perfer hot or cold place?
              """)
        print("1. Hot")
        print("2. Cold")
        isHot = input("Hot or Cold: ")
        logic = QuizLogic()
        isHot = logic.get_valid_isHot(isHot)
        while isHot == None:
            print("Invalid input, please enter Hot or Cold")
            isHot = input("Hot or Cold: ")
            isHot = logic.get_valid_isHot(isHot)
        return isHot

    def get_new_quiz(self):
        '''
        This function creates a new quiz with user's budget and save it.
        '''
        budget = self.get_budget()
        couple = self.get_couple()
        family = self.get_family()
        adventure = self.get_adventure()
        isHot = self.get_isHot()
        quiz = Quiz(self.userId, budget, couple, family, adventure, isHot)
        save = self.save_new_quiz(quiz)
        print("Quiz answer saved")
        return quiz

    def save_new_quiz(self, quiz):
        '''
        This function saves a new quiz answer in the database, using a funciton from QuizLogic. 
        '''
        logic = QuizLogic()
        return logic.save_new_quiz(quiz)
