from grpcService.client import run
from logic.countryLogic import CountryLogic
from logic.userLogic import UserLogic
from ui.favoritesUi import FavoritesUI
from ui.menu import Menu

LINE = '------------------------------------------'


class LocationDetailPageUI:
    """
    A class representing the user interface for displaying and managing details of a location and adding/removing it from favorites.

    Attributes:
        user_id (int): The user ID for which the location details are displayed.
        country_logic (CountryLogic): An instance of the CountryLogic class for interacting with country data.
        user_logic (UserLogic): An instance of the UserLogic class for user-related operations.

    Methods:
        add_to_favourites(self, favourites, country_info): Add a location to the user's favorites and provide options for further actions.
        remove_from_favourites(self, favourites, country_info): Remove a location from the user's favorites and provide options for further actions.
        show_detail_page(self, country_id): Display details of a specific location, allowing the user to add or remove it from favorites.
    """
    def __init__(self, user_id) -> None:
        self.user_id = user_id
        self.country_logic = CountryLogic()
        self.user_logic = UserLogic()
    
    def add_to_favourites(self, favourites, country_info):
        """
        Add a location to the user's favorites and provide options for further actions.

        Args:
            favourites (str): The user's current list of favorite locations.
            country_info (dict): Information about the location to be added.

        Returns:
            None
        """
        favourites += f"{country_info['Id']}"
        self.user_logic.edit_user_info(self.user_id, '[countryId]', favourites)
        intro = f"{country_info['Country']} added to favourites list!"
        options = [f"Remove {country_info['Country']} from favourites", "See my favourites"]
        menu = Menu(intro, options)
        selection = menu.get_selection()
        if selection <  0:
            return
        if selection == 0:
            self.remove_from_favourites(favourites, country_info)
        if selection == 1:
            ui = FavoritesUI(self.user_id)
            show_favorites = ui.show_favorites()
    
    def remove_from_favourites(self, favourites, country_info):
        """
        Remove a location from the user's favorites and provide options for further actions.

        Args:
            favourites (str): The user's current list of favorite locations.
            country_info (dict): Information about the location to be removed.

        Returns:
            None
        """
        favourites_list = []
        for element in favourites:
            favourites_list.append(element)
        favourites_list.remove(f"{country_info['Id'].strip()}")
        favourites = ""
        for element in favourites_list:
            favourites += f"{element}"
        self.user_logic.edit_user_info(self.user_id, '[countryId]', favourites)
        intro = f"{country_info['Country']} removed from favourites list!"
        options = [f"Add {country_info['Country']} to favourites", "See my favourites"]
        menu = Menu(intro, options)
        selection = menu.get_selection()
        if selection <  0:
            return
        if selection == 0:
            self.add_to_favourites(favourites, country_info)
        if selection == 1:
            ui = FavoritesUI(self.user_id)
            show_favorites = ui.show_favorites()
            
    def show_detail_page(self, country_id):
        """
        Display details of a specific location and allow the user to add or remove it from favorites.

        Args:
            country_id (int): The ID of the location for which details are displayed.

        Returns:
            None
        """
        
        while True:
            user_info = self.user_logic.get_user_info(self.user_id)
            favourites = user_info['[countryId]']
            print("Destination detail page:")
            print(LINE)
            country_info, error_message = self.country_logic.get_country_by_id(country_id)
            if error_message:
                print(error_message)
            else:
                intro = f"Country: {country_info['Country']}\nCity: {country_info['City']}\nDescription: {country_info['Description']}\nisHot: {country_info['IsHot']}\nAverage price: {country_info['AvgPrice']}\nHotels: {country_info['Hotels']}\n{run(float(country_info['Lat'][0]), float(country_info['Lon']))}"
                options = []
                favourites_list = []
                for element in favourites:
                    favourites_list.append(element)
                if country_info['Id'] in favourites_list:
                    options = ["Remove from favourites"]
                else:
                    options = ["Add to favourites"]
                menu = Menu(intro, options)
                selection = menu.get_selection()

                if selection < 0:
                    break
                if options[selection] == "Add to favourites":
                    self.add_to_favourites(favourites, country_info)
                elif options[selection] == "Remove from favourites":
                    self.remove_from_favourites(favourites, country_info)
