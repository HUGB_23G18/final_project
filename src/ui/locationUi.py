from collections import OrderedDict
from operator import getitem

from logic.countryLogic import CountryLogic
from ui.locationDetailPageUi import LocationDetailPageUI
from ui.menu import Menu

LINE = '------------------------------------------'

class LocationUI:
    '''
    This class provides a user interface for displaying a list of available travel destinations. Users can filter destinations by various criteria, including all destinations, warm destinations, cold destinations, and destinations by price. 
    It utilizes the 'get_all_countries' function from the 'country_logic' module to retrieve country information.
    '''
    def __init__(self, user_id) -> None:
        self.user_id = user_id
        self.destination_num = 0

    def show_destinations(self):
        '''
        This function displays a list of available travel destinations and allow user to filter by what they want. 
        '''
        logic = CountryLogic
        while True:
            countries, error_message = logic.countrie_list_to_dict(self)
            self.destination_num = len(countries)
            if error_message:
                print(error_message)
            else:

                options = ["All", "Warm", "Cold", "Price", "Search by name", "Search by price"]
                menu = Menu("Please choose a category: ", options)
                selection = menu.get_selection()
                
                if selection < 0:
                    break
                if options[selection] == "All":
                    
                    for country_id, country_info in countries.items():
                        print(f"{country_id}: {country_info['Country']}, {country_info['City'] }")
                            
                elif options[selection] == "Warm":
                    for country_id, country_info in countries.items():
                        if country_id == "countryId":
                            continue
                        
                        if country_info['IsHot'] == True:
                            print(f"{country_id}: {country_info['Country']}, {country_info['City'] }")
                            
                elif options[selection] == "Cold":
                    for country_id, country_info in countries.items():
                        if country_id == "countryId":
                            continue
                        if country_info['IsHot'] == False:
                            print(f"{country_id}: {country_info['Country']}, {country_info['City'], }")
                elif options[selection] == "Price":
                    res = OrderedDict(sorted(countries.items(), key=lambda x: getitem(x[1], 'AvgPrice')))
                    for country_id, country_info in res.items():
                        if country_id == "countryId":
                            continue
                        print(f"{country_id}: {country_info['Country']}, {country_info['City'] }, {country_info['AvgPrice']}")
                elif options[selection] == "Search by name":
                    search_name = input("Enter the country name: ").strip().lower()
                    found = False
                    for country_id, country_info in countries.items():
                        if country_id == "countryId":
                            continue
                        if search_name in country_info['Country'].lower() or search_name in country_info['City'].lower():
                            print(f"{country_id}: {country_info['Country']}, {country_info['City']}")
                            found = True
                    if not found:
                        print(f"Sorry, no country that fits {search_name} was found")
                

                # Search by price, display countries that are closest to the price range, and display the price range.
                elif options[selection] == "Search by price":
                    search_price = float(input("Enter the price: ").strip())
                    lower_bound = search_price - 50.000
                    upper_bound = search_price + 50.000
                    found = False
                    for country_id, country_info in countries.items():
                        if country_id == "countryId":
                            continue
                        if lower_bound <= float(country_info['AvgPrice']) <= upper_bound:
                            print(f"{country_id}: {country_info['Country']}, {country_info['City']}")
                            found = True
                    if not found:
                        print(f"No countries with an average price close to {search_price:.0f} found.")

                print(LINE)
                user_input = input("Press 'b' to go back, or enter country id : ").lower()
                if user_input == 'b':
                    pass
                else:
                    try:
                        
                        if int(user_input) > self.destination_num:
                            print("Not a valid input, try again")
                        else:
                            print(LINE)
                            detail_page = LocationDetailPageUI(self.user_id)
                            detail_page.show_detail_page(user_input)
                    except:
                        print("Not a valid input, try again")
