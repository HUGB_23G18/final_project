from ui.createAccount import CreateAccount
from ui.editAccount import ProfileEditor
from ui.homeUi import HomeUI
from ui.menu import Menu
from ui.quiz import QuizUI
from ui.unauthoricedUi import Unauthoriced_UI
from utils.utilities import clear_terminal

# Main python file (Just for setting up the repository)


def main():
    userId = None
    running = True
    while running:
        if userId != None:
            ui = HomeUI(userId)
            menu = ui.input_prompt()
            if menu == "q":
                userId = None
        else:
            ui = Unauthoriced_UI()
            userId = ui.input_prompt()
            if userId == "q":
                running = False


if __name__ == "__main__":
    main()
