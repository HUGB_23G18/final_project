import csv
import os
from model.quiz import Quiz
from data.DataUtils import read_from_CSV_file


class QuizData:
    '''
    QuizData is a class that provides methods for managing quiz data stored in the database.
    It allows you to read quiz answers, save new quizzes, and retrieve quizzes by user ID.
    '''

    def __init__(self) -> None:
        self.file = "database/QuizDB.csv"

    def read_all_quizzes(self):
        '''
        Reads all quiz answers from the given file
        '''
        if not os.path.exists(self.file):
            self.file = "src/database/QuizDB.csv"
        ret_list = []
        with read_from_CSV_file(self.file) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                ret_list.append(Quiz(
                    row['userId'],
                    row['budget'],
                    row['couple'],
                    row['family'],
                    row['adventure'],
                    row['isHot']
                ))
            return ret_list

    def save_new_quiz(self, quiz):
        '''
        Saves a new quiz answer in the CSV file or updates an existing one if the user ID exists.
        Returns True if it succeeded, False if the quiz database file is not found.
        '''
        if not os.path.exists(self.file):
            self.file = "src/database/QuizDB.csv"

        try:
            # Read existing data
            existing_data = []
            if os.path.isfile(self.file):
                with open(self.file, "r", newline="", encoding="utf-8") as csvfile:
                    reader = csv.DictReader(csvfile)
                    existing_data = list(reader)

            # Check if the user ID exists in the existing data
            for existing_record in existing_data:
                if existing_record['userId'] == quiz.userId:
                    # Overwrite the existing record
                    existing_record['budget'] = quiz.budget
                    existing_record['couple'] = quiz.couple
                    existing_record['family'] = quiz.family
                    existing_record['adventure'] = quiz.adventure
                    existing_record['isHot'] = quiz.isHot
                    with open(self.file, "w", newline="", encoding="utf-8") as csvfile:
                        writer = csv.DictWriter(
                            csvfile, fieldnames=existing_data[0].keys())
                        writer.writeheader()
                        writer.writerows(existing_data)
                    return True

            # If user ID doesn't exist, append the new record
            with open(self.file, "a", newline="", encoding="utf-8") as csvfile:
                fieldnames = ["userId", "budget",
                              "couple", "family", "adventure", "isHot"]
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writerow({
                    'userId': quiz.userId,
                    'budget': quiz.budget,
                    'couple': quiz.couple,
                    'family': quiz.family,
                    'adventure': quiz.adventure,
                    'isHot': quiz.isHot
                })
            return True
        except FileNotFoundError:
            print("Error: Quiz database file not found")
            return False

    def get_quiz_by_userId(self, userId):
        '''
        Gets a quiz answer from the given file by userId
        '''
        if not os.path.exists(self.file):
            self.file = "src/database/QuizDB.csv"
        with open(self.file, "r", newline="", encoding="utf-8") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if row['userId'] == userId:
                    return Quiz(
                        row['userId'],
                        row['budget'],
                        row['couple'],
                        row['family'],
                        row['adventure'],
                        row['isHot']
                    )
            return None
