import ast
import csv
import os

from data.DataUtils import edit_CSV_File, read_from_CSV_file
# Import relevant classes and functions from other files
from model.createAccount import Account
from data.DataUtils import edit_CSV_File, read_from_CSV_file


class AccountData:
    ''' 
    AccountData is a class that provides methods for managing user accounts stored in our database.
    It allows you to read user accounts, add new accounts, authenticate users, edit user information,
    retrieve user information by user ID and username, check if an email exists in the database,
    and save a new password for a user based on their email.
    '''

    def __init__(self) -> None:
        self.database_file_location = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../", "database/UserDB.csv")
        self.file = "src/database/UserDB.csv"
        # Get the current directory (where dummy_utilities.py is located)
        current_directory = os.path.dirname(os.path.abspath(__file__))
        # Navigate to the database directory and open UserDB.csv
        self.filepath = os.path.join(current_directory, "..", "database/UserDB.csv")

    def read_all_accounts(self):
        '''
        This function retrieves and parses user data from the UserDB.csv file and returns it as a list of Account objects. 
        '''
        ret_list = []
        with read_from_CSV_file(self.filepath) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                ret_list.append(Account(
                    row['userId'], row['username'], row['password'], row['email'], ast.literal_eval(row['[countryId]'])))
            return ret_list

    def add_account(self, account):
        '''
        Adds an account to the UserDB.csv file. This method appends the provided account data to the UserDB.csv file, 
        effectively registering a new user in the system. 
        It uses the current directory to locate the UserDB.csv file, ensuring the new account is added to the correct database.
        '''
        # Get the current directory (where dummy_utilities.py is located)
        current_directory = os.path.dirname(os.path.abspath(__file__))
        # Navigate to the database directory and open UserDB.csv
        filepath = os.path.join(current_directory, "..", "database/UserDB.csv")
        if not os.path.exists(self.filepath):
            self.file = "database/UserDB.csv"
        with open(filepath, mode='a', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(account)

    def authenticate(self, username, password):
        '''
        Authenticates a user based on username and password.
        Returns the userId if authentication is successful, otherwise returns None.
        '''
        if not os.path.exists(self.filepath):
            self.file = "src/database/UserDB.csv"
        try:
            with open(self.filepath, "r", newline="", encoding="utf-8") as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    if row['username'] == username and row['password'] == password:
                        # Authentication successful, return userId
                        return row['userId']
        except FileNotFoundError:
            print("Error: User database file not found")
            return None
        return None  # Authentication failed, return None

    def edit_user_info(self, user_info, field_edit, new_val):
        '''
        Edits user information in the UserDB.csv file.
        If user_info matches a user and field_edit is 'username' or 'password', the specified field is updated with the new_val.
        '''
        edited_data = []
        fieldnames = ['userId', 'username', 'password', 'email', "[countryId]"]
        with read_from_CSV_file('UserDB.csv') as file:
            reader = csv.DictReader(file, fieldnames=fieldnames)
            for row in reader:
                edited_row = []
                if user_info is not None and row['userId'] == user_info["id"]:
                    if field_edit == 'username':
                        row["username"] = new_val
                    elif field_edit == 'password':
                        row["password"] = new_val
                    elif field_edit == '[countryId]':
                        row["[countryId]"] = new_val
                for value in row.values():
                    edited_row.append(value)
                edited_data.append(edited_row)
        edit_CSV_File('UserDB.csv', edited_data)

    def get_user_info(self, user_id: str) -> None:
        '''
        A function that takes in a str user id from the login and gets all the info of the user and returns it as a dictionary.
        It returns a tuple that either return a dictionary of the user and a None error message but if user was not found
        it returns None in user info and an error message.
        '''
        if not os.path.exists(self.file):
            self.file = "database/UserDB.csv"
        try:
            user_info = {}

            # Tries to open the database file and finds the user and adds it to the user info dictionary
            with read_from_CSV_file('UserDB.csv') as file:
                for line in file:
                    data = line.strip().split(',')

                    if str(data[0]) == str(user_id):
                        user_info['id'] = data[0]
                        user_info['username'] = data[1]
                        user_info['password'] = data[2]
                        user_info['email'] = data[3]
                        user_info['[countryId]'] = data[4]
                        break
            file.close()

            # Checks if user_info is emtpy
            dict_check = bool(user_info)

            # If user info is empty it raises an exception and alerts that the user has not been found.
            if dict_check:
                return (user_info, None)

            else:
                return (None, "No user with user id \'" + str(user_id) + "\' was found!")
        except FileNotFoundError:
            return (None, "Error: User database file not found")

    def get_user_info_from_username(self, username: str) -> None:
        '''
        A function that takes in a str username from the login and gets all the info of the user and returns it as a dictionary.
        It returns a tuple that either return a dictionary of the user and a None error message but if user was not found
        it returns None in user info and an error message.
        '''
        user_info = {}

        # Tries to open the database file and finds the user and adds it to the user info dictionary
        with read_from_CSV_file('UserDB.csv') as file:
            for line in file:
                data = line.strip().split(',')

                if data[1] == username:
                    user_info['id'] = data[0]
                    user_info['username'] = data[1]
                    user_info['password'] = data[2]
                    user_info['email'] = data[3]
                    user_info['[countryId]'] = data[4]
                    break
        file.close()

        # Checks if user_info is emtpy
        dict_check = bool(user_info)

        # If user info is empty it raises an exception and alerts that the user has not been found.
        if dict_check:
            return (user_info, None)
        else:
            return (None, "No user with username \'" + username + "\' was found!")

    def is_email_in_db(self, email):
        '''
        Checks if an email exists in the user database.
        Returns True if the email exists, otherwise returns False.
        '''
        if not os.path.exists(self.file):
            self.file = "database/UserDB.csv"
        try:
            with open(self.file, "r", newline="", encoding="utf-8") as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    if row['email'] == email:
                        return True  # Email exists in the database
        except FileNotFoundError:
            print("Error: User database file not found")
        return False  # Email not found in the database

    def save_new_password(self, email, password):
        '''
        Saves a new password for the user with the given email.
        Returns the UserId if the password was saved successfully, otherwise returns None.
        '''
        if not os.path.exists(self.file):
            self.file = "database/UserDB.csv"
        try:
            # Read existing data
            existing_data = []
            if os.path.isfile(self.file):
                with open(self.file, "r", newline="", encoding="utf-8") as csvfile:
                    reader = csv.DictReader(csvfile)
                    existing_data = list(reader)

            # Check if the email exists in the existing data
            for existing_record in existing_data:
                if existing_record['email'] == email:
                    # Overwrite the existing record
                    existing_record['password'] = password
                    with open(self.file, "w", newline="", encoding="utf-8") as csvfile:
                        writer = csv.DictWriter(
                            csvfile, fieldnames=existing_data[0].keys())
                        writer.writeheader()
                        writer.writerows(existing_data)
                    return existing_record['userId']  # Return UserId

            # If email doesn't exist, return None
            return None
        except FileNotFoundError:
            print("Error: User database file not found")
            return None
