import csv
import os
from datetime import datetime
from data.DataUtils import read_from_CSV_file, getting_new_id_from_CSV, write_to_CSV_File
from logic.userLogic import UserLogic


class BookingData:
    def __init__(self):
        current_directory = os.path.dirname(os.path.abspath(__file__))
        # Navigate to the database directory and open UserDB.csv
        self.filepath = os.path.join(
            current_directory, "..", "database/BookingDB.csv")
        self.userDBfilepath = os.path.join(
            current_directory, "..", "database/UserDB.csv")

    def get_all_bookings(self):
        '''
        This function retrieves and parses booking data from the BookingDB.csv and 
        returns it in a tuble with an error (if there is one)
        '''
        bookings = {}
        with read_from_CSV_file(self.filepath) as file:
            for line in file:
                data = line.strip().split(',')
                id = data[0]

                booking_info = {}
                booking_info['userId'] = data[1]
                booking_info['countryId'] = data[2]
                booking_info['Date booked'] = data[3]
                booking_info['price'] = data[4]

                bookings[id] = booking_info
            file.close()

        dict_check = not bool(bookings)

        if dict_check:
            return (None, "No bookings are in the database!")
        else:
            return bookings

    def get_booking_by_id(self, bookingId):
        '''
        This function gets the info of the booking depending on the id given.
        It returns a tuble with either a dict with nothing or nothing with an error code.
        '''
        booking = {}
        with read_from_CSV_file(self.filepath) as file:
            for line in file:
                data = line.strip().split(',')

                if str(data[0]) == str(bookingId):
                    booking['id'] = data[0]
                    booking['userId'] = data[1]
                    booking['countryId'] = data[2]
                    booking['datetime'] = data[3]
                    booking['price'] = data[4]
                    break
        file.close()

        dict_check = bool(booking)

        # If booking info is empty it raises an exception and alerts that the booking has not been found.
        if dict_check:
            return (booking, None)

        else:
            return (None, "No booking with id \'" + str(bookingId) + "\' was found!")

    def book(self, userId, countryId, price):
        '''
        This function creates a new booking, with the current date and time and appends it to the database.
        '''
        newId = getting_new_id_from_CSV(self.filepath)
        current_datetime = datetime.now()

        # Changes the formatting of the time and date, Time: hours:minutes, Date: dd-mm-yy
        formatted_time = current_datetime.strftime("%H:%M")
        formatted_date = current_datetime.strftime("%d-%m-%Y")

        formatted_datetime = formatted_date + " / " + formatted_time

        newbooking = [newId, userId, countryId, formatted_datetime, price]

        # Adds the newly created booking to the database
        write_to_CSV_File(self.filepath, newbooking)

        # Sends a confirmation email to the user.
        self.send_confirmation_email(userId, newId, formatted_datetime)

    def send_confirmation_email(self, userId: int, bookingId, datetime):
        '''
        This function gets the email of the user from the userdatabase
        '''
        userEmail = None
        userName = None
        with read_from_CSV_file(self.userDBfilepath) as file:
            for line in file:
                data = line.strip().split(',')

                if str(data[0]) == str(userId):
                    userName = data[1]
                    userEmail = data[3]
                    break
        file.close()

        if userEmail == None:
            return "User with id " + userId + "was not found!"

        body = "Dear " + userName + ",\n\nThis is your confirmation email for you newly booked trip.\nYour booking id: " + \
            bookingId + "\nBooked on: " + datetime + \
            "\n\nPlease enjoy your time on your trip and hopefully we'll see you again."
        userlogic = UserLogic()
        userlogic.send_custom_email(userEmail, "Booking Confirmation.", body)
