import csv
import os

database_file_location = os.path.join(os.path.dirname(
    os.path.abspath(__file__)), "../", "database")

def getting_new_id_from_CSV(inputFile):
    '''
    Retrieves the latest ID from a CSV file, useful for generating unique IDs.
    '''
    # Navigate to the database directory and open UserDB.csv
    filePath = os.path.join(database_file_location, inputFile)

    with open(filePath, 'r') as csvFile:
        reader = csv.reader(csvFile)
        lines = list(reader)
        if len(lines) > 1:
            return lines[-1][0]
        else:
            return None

def write_to_CSV_File(inputFile, dataToWrite):
    '''
    Appends data to a CSV file, used for adding new data to the file.
    '''
    # Navigate to the database directory and open UserDB.csv
    filePath = os.path.join(database_file_location, inputFile)
    with open(filePath, mode='a', newline='') as csvfile:  # Use 'a' for append mode
        writer = csv.writer(csvfile)
        writer.writerow(dataToWrite)


def read_from_CSV_file(inputFile):
    '''
    Opens a CSV file for reading, providing access to its data.
    '''
    try:
        filePath = os.path.join(database_file_location, inputFile)
        return open(filePath, 'r')
    except FileNotFoundError:
        print("Error: User database file not found")
    except Exception as e:
        print(f"An error occurred: {str(e)}")


def edit_CSV_File(inputFile, dataToWrite):
    '''
    Edits a CSV file, used to update the content of the database.
    '''
    # Navigate to the database directory and open UserDB.csv
    filePath = os.path.join(database_file_location, inputFile)
    with open(filePath, mode='w', newline='') as csvfile:  # Use 'w' for append mode
        writer = csv.writer(csvfile)
        writer.writerows(dataToWrite)
