import csv
import os


class PackageData:
    def __init__(self) -> None:
        self.file = "database/CountryDB.csv"

    def get_suggested_package(self, quiz):
        '''
        This function gets suggested packages from the database based on the user's quiz answers.
        '''
        if not os.path.exists(self.file):
            self.file = "src/database/CountryDB.csv"

        countries = []
        with open(self.file, mode='r', encoding='utf-8') as file:
            reader = csv.DictReader(file)
            for row in reader:
                countries.append(row)

        suggested_packages = []

        for country in countries:
            if ((str(quiz.couple).lower()) == (country['couple'].lower()) and
                (str(quiz.family).lower()) == (country['family'].lower()) and
                (str(quiz.adventure).lower()) == (country['adventurous'].lower()) and
                (str(quiz.isHot).lower()) == (country['isHot'].lower()) and
                    int(quiz.budget) >= int(country['AvgPrice'])):
                suggested_packages.append(country)

        return suggested_packages

    def save_selected_package(self, userId, quiz):
        pastTrips_file = "database/PastTrips.csv"
        if not os.path.exists(pastTrips_file):
            pastTrips_file = "src/database/PastTrips.csv"

        with open(pastTrips_file, mode='a', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([userId, quiz])
        print("Package saved successfully")

    def get_all_packages(self):
        '''
        This function gets all packages from the database.
        '''
        if not os.path.exists(self.file):
            self.file = "src/database/CountryDB.csv"

        countries = []
        with open(self.file, mode='r', encoding='utf-8') as file:
            reader = csv.DictReader(file)
            for row in reader:
                countries.append(row)
        return countries
