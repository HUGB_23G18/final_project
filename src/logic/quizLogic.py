from data.PackageData import PackageData
from data.QuizData import QuizData
import grpc
from grpcHelpers import getQuizDatabase_pb2 as getQuizDB_pb2
from grpcHelpers.getQuizDatabase_pb2_grpc import QuizServiceStub
from logic.countryLogic import CountryLogic


class QuizLogic:
    '''
    QuizLogic is a class that provides logic for validating and saving quiz data.
    '''

    def __init__(self) -> None:
        pass

    def get_valid_budget(self, budget):
        '''
        The function checks if the provided 'budget' value is valid by ensuring it's a non-negative integer. 
        If 'budget' is an empty string, less than zero, or not a valid integer, it returns False.
        Otherwise, it returns True.
        '''
        if budget == "":
            return False
        try:
            budget = int(budget)
            if budget < 0:
                return False
            return True
        except ValueError:
            return False

    def save_new_quiz(self, quiz):
        '''
        Saves a new quiz using QuizData.
        '''
        quiz_data = QuizData()
        return quiz_data.save_new_quiz(quiz)

    def get_valid_fancy(self, fancy):
        '''
        The function checks if the provided 'fancy' value is valid by ensuring it's an integer between 1 and 5. 
        If 'fancy' is an empty string, less than 1, greater than 5, or not a valid integer, it returns False.
        Otherwise, it returns True.
        '''
        if fancy == "":
            return False
        try:
            fancy = int(fancy)
            if fancy < 1 or fancy > 5:
                return False
            return True
        except ValueError:
            return False

    def get_valid_couple(self, couple):
        '''
        The function checks if the provided 'couple' value is valid by ensuring it's either yes or no.
        If 'couple' is an empty string, or not yes or no, it returns False.
        Otherwise, it returns True.
        '''
        if couple == "":
            return None
        try:
            couple = int(couple)
            if couple == 1:
                couple = True
                return couple
            elif couple == 2:
                couple = False
                return couple
            else:
                return None
        except:
            try:
                couple = str(couple)
                couple = couple.lower()
                if couple == "yes" or couple == "y":
                    couple = True
                    return couple
                elif couple == "no" or couple == "n":
                    couple = False
                    return couple
                else:
                    return None
            except:
                return None

    def get_valid_family(self, family):
        '''
        The function checks if the provided 'family' value is valid by ensuring it's either yes or no. 
        If 'family' is an empty string, or not yes or no, it returns False.
        Otherwise, it returns True.
        '''
        if family == "":
            return None
        try:
            family = int(family)
            if family == 1:
                family = True
                return family
            elif family == 2:
                family = False
                return family
            else:
                return None
        except:
            try:
                family = str(family)

                family = family.lower()
                if family == "yes" or family == "y":
                    family = True
                    return family
                elif family == "no" or family == "n":
                    family = False
                    return family
                else:
                    return None
            except:
                return None

    def get_valid_adventure(self, adventure):
        '''
        The function checks if the provided 'adventure' value is valid by ensuring it's either yes or no. 
        If 'adventure' is an empty string, or not yes or no, it returns False.
        Otherwise, it returns True.
        '''
        if adventure == "":
            return None
        try:
            adventure = int(adventure)
            if adventure == 1:
                adventure = True
                return adventure
            elif adventure == 2:
                adventure = False
                return adventure
            else:
                return None
        except:
            try:
                adventure = str(adventure)
                adventure = adventure.lower()
                if adventure == "yes" or adventure == "y":
                    adventure = True
                    return adventure
                elif adventure == "no" or adventure == "n":
                    adventure = False
                    return adventure
                else:
                    return None
            except:
                return None

    def get_valid_isHot(self, isHot):
        if isHot == "":
            return None
        try:
            isHot = int(isHot)
            if isHot == 1:
                isHot = True
                return isHot
            elif isHot == 2:
                isHot = False
                return isHot
            else:
                return None
        except:
            try:
                isHot = str(isHot)
                isHot = isHot.lower()
                if isHot == "hot" or isHot == "h":
                    isHot = True
                    return isHot
                elif isHot == "cold" or isHot == "c":
                    isHot = False
                    return isHot
                else:
                    return None
            except:
                return None

    def quiz_data_to_dict(self):
        # Create a gRPC channel to the server
        channel = grpc.insecure_channel('localhost:50051')

        # Create a gRPC stub for the QuizService
        stub = QuizServiceStub(channel)

        try:
            # Call the GetQuizData gRPC method to fetch quiz data
            response = stub.GetQuizData(getQuizDB_pb2.EmptyMessage())

            # Extract the list of quizzes from the response
            all_quizzes = response.quizzes
            quizzes = {}

            # Iterate through the list of quiz protobuf messages
            for quiz_proto in all_quizzes:
                user_id = quiz_proto.userId

                # Create a dictionary to represent the quiz
                quiz_info = {
                    'UserId': user_id,
                    'Budget': quiz_proto.budget,
                    'Couple': quiz_proto.couple,
                    'Family': quiz_proto.family,
                    'Adventure': quiz_proto.adventure,
                    'IsHot': quiz_proto.isHot
                }

                # Add the quiz to the dictionary using its UserId as the key
                quizzes[user_id] = quiz_info

            # Check if the quizzes dictionary is empty
            dict_check = not bool(quizzes)

            if dict_check:
                return (None, "There are no quizzes in the database.")
            else:
                return (quizzes, None)
        except grpc.RpcError as e:
            # Handle the gRPC error, for instance:
            return (None, f"An error occurred: {e.details()}")

    def get_suggested_packages(self, quiz):
        Data = PackageData()
        packages = Data.get_suggested_package(quiz)
        return packages

    def save_selected_package(self, userId, quiz):
        Data = PackageData()
        Data.save_selected_package(userId, quiz)
        return True

    def get_all_packages(self):
        Data = PackageData()
        packages = Data.get_all_packages()
        return packages
