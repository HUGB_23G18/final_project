
class Quiz:
    def __init__(self, userId, budget) -> None:
        '''
        Makes a model of quiz answers
        '''
        self.userId = userId
        self.budget = budget