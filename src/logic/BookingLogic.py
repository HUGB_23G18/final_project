from data.BookingData import BookingData

class BookingLogic:
    '''
    Provides logic for managing bookings, including retrieving, creating, and confirming bookings.
    '''
    def __init__(self):
        self.booking_data = BookingData()

    def get_all_bookings(self):
        '''
        Retrieves all booking information.
        '''
        return self.booking_data.get_all_bookings()

    def get_booking_by_id(self, booking_id):
        '''
        Retrieves booking information by its bookingID.
        '''
        return self.booking_data.get_booking_by_id(booking_id)

    def book(self, user_id, country_id, price):
        '''
        Creates a new booking for a user.
        '''
        self.booking_data.book(user_id, country_id, price)

    def send_confirmation_email(self, user_id, booking_id, datetime):
        '''
        Sends a confirmation email for a booking.
        '''
        return self.booking_data.send_confirmation_email(user_id, booking_id, datetime)
