import grpc
from grpcHelpers import getCountryDB_pb2
from grpcHelpers.getCountryDB_pb2_grpc import CountryServiceStub

from data.DataUtils import read_from_CSV_file
from grpcService.client import run
from ui.menu import Menu


# def print_country_info(country_list):
#     options = country_list
#     menu = Menu("Here are the available countries,\nPlease choose a country to see more about it.", options)
#     selection = menu.get_selection()
#     if selection < 0:
#         return "q"
#     selection = int(selection) + 1
#     selected_country, error_message = get_country_by_id(str(selection))
#     if error_message:
#         print(error_message)
#     else:
#         print(f"{selected_country['Country']}")
#         print(f"{selected_country['City']}")
#         print(f"{selected_country['Description']}")
#         print(f"{selected_country['AvgPrice']}")
#         for i in selected_country['Hotels'][0]:
#             print(i)
#         run(float(selected_country['Lat'][0]), float(selected_country['Lon']))


class CountryLogic:
    '''
    CountryLogic is a class that provides logic for getting destination information.
    '''
    def __init__(self) -> None:
        pass

    def get_country_by_id(self, country_id):
        '''
        Returns a tuble with the country that has the specifed id in a dictionary and a error message if there is one.
        If there is no country, it resturns a tuble with None and a error message that says that the country with the specified id was not found.
        '''
        countryInfo = {}

        with read_from_CSV_file('CountryDB.csv') as countryCSVFile:
            for line in countryCSVFile:
                data = line.strip().split(',')
                
                if data[0] == country_id:
                    countryInfo['Id'] = data[0]
                    countryInfo['Country'] = data[1]
                    countryInfo['City'] = data[2]
                    countryInfo['Description'] = data[3]
                    countryInfo['IsHot'] = data[4]
                    countryInfo['AvgPrice'] = data[5]
                    countryInfo['Hotels'] = data[6]
                    countryInfo['Lat'] = data[7],
                    countryInfo['Lon'] = data[8]
            countryCSVFile.close()

        dict_check = not bool(countryInfo)

        if dict_check:
            return (None, "No country with the id \'" + country_id + "\' was found!")
        else:
            return (countryInfo, None)
    #we use this function for the grpc to get the data and send with grpc
    def get_all_countries(self):
        '''
        A function that returns all the countries in the database in a tuble and a None for the error message. 
        If there are no countries in the database the function returns a tuble with None and a error message.
        '''
        countries = {}

        with read_from_CSV_file('CountryDB.csv') as countryCSVFile:
            for line in countryCSVFile:
                data = line.strip().split(',')
                country_id = data[0]
                hotels_list = data[6].split('/')

                # Create a dictionary to represent the country
                countryInfo = {
                    'Id': data[0],
                    'Country': data[1],
                    'City': data[2],
                    'Description': data[3],
                    'IsHot': data[4],
                    'AvgPrice': data[5],
                    'Hotels': hotels_list, 
                    'Lat': data[7],
                    'Lon': data[8]

                }

                # Add the country to the dictionary using its ID as the key
                countries[country_id] = countryInfo

        dict_check = not bool(countries)

        if dict_check:
            return (None, "There are no countries in the database.")
        else:
            return (countries, None)

    def countrie_list_to_dict(self):
        # Create a gRPC channel to the server
        channel = grpc.insecure_channel('localhost:50051')
        
        # Create a gRPC stub for the CountryService
        stub = CountryServiceStub(channel)
        
        try:
            # Call the get_country_list gRPC method to fetch country data
            response = stub.get_country_list(getCountryDB_pb2.EmptyMessage())
            
            # Extract the list of countries from the response
            all_countries = response.countries
            countries = {}
            
            # Iterate through the list of country protobuf messages
            for country_proto in all_countries:
                country_id = country_proto.countryId
                hotels_list = country_proto.hotels.split('/')
                
                # Create a dictionary to represent the country
                country_info = {
                    'Id': country_id,
                    'Country': country_proto.country,
                    'City': country_proto.city,
                    'Description': country_proto.description,
                    'IsHot': country_proto.isHot,
                    'AvgPrice': country_proto.avgPrice,
                    'Hotels': hotels_list,
                    'Lat': country_proto.lat,
                    'Lon': country_proto.lon,
                }
                
                # Add the country to the dictionary using its ID as the key
                countries[country_id] = country_info
                
            # Check if the countries dictionary is empty
            dict_check = not bool(countries)

            if dict_check:
                return (None, "There are no countries in the database.")
            else:
                return (countries, None)
        except grpc.RpcError as e:
            print("Error:", e)

