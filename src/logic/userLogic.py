import random
import smtplib

from data.AccountData import AccountData
from utils.exceptions import NotFoundException
from data.DataUtils import read_from_CSV_file


class UserLogic:
    '''
    UserLogic is a class that provides logic for user-related operations such as authentication, 
    information retrieval, and email handling.
    '''
    def __init__(self) -> None:
        pass

    def get_user_info(self, user_id: str) -> None:
        '''
        A function that uses the get_user_info function from AccountData to get all the info of the user and returns it as a dictionary.
        It returns a tuple that either return a dictionary of the user and a None error message but if user was not found
        it returns None in user info and an error message.
        '''
        data = AccountData()
        user_info, error_mes = data.get_user_info(user_id)
        if user_info == None:
            raise NotFoundException(error_mes)
        else:
            return user_info
        
    def get_all_users(self):
        '''
        A function that takes all the users in the database and return them in a dictionary
        '''
        all_users = {}

        #  Tries to open the database file, finds all the users and adds them to the list
        with read_from_CSV_file('UserDB.csv') as file:
            for line in file:
                data = line.strip().split(',')
                id = data[0]

                user_info = {}
                user_info['username'] = data[1]
                user_info['password'] = data[2]
                user_info['email'] = data[3]
                user_info['[countryId]'] = data[4]

                all_users[id] = user_info
            file.close()

        dict_check = not bool(all_users)

        if dict_check:
            return (None, "No user are in the database!")
        else:
            return all_users


    def check_for_duplicate_username(self, username):
        '''
        The function checks the database if another user has that username, if so it return True, if nothing is found, then False.
        '''
        result = False
        data = AccountData()
        user_info, error_mes = data.get_user_info_from_username(username)
        
        if user_info:
            result = True
        elif error_mes:
            result = False

        return result


    def authenticate(self, username, password):
        '''
        Returns the userId if the username and password match a user in the user database, None otherwise
        '''
        data = AccountData()
        userId = data.authenticate(username, password)
        return userId


    def edit_user_info(self, user_id, field_edit, new_val):
        data = AccountData()
        user_info, error_mes = data.get_user_info(user_id)
        if user_info == None:
            raise NotFoundException(error_mes)
        data.edit_user_info(user_info, field_edit, new_val)


    def validate_email(self, email):
        '''
        Validates an email address using a function in AccountData and sends a verification code using the send_email function below.
        '''
        data = AccountData()
        if data.is_email_in_db(email):
            random_number = self.send_email(email)
            return random_number


    def send_email(self, email):
        '''
        Constructs an email to send to the provided email address.
        '''
        gmail_user = 'travel8tor23@gmail.com'
        gmail_password = 'xgmz rcrv qofn cwmj'

        sent_from = gmail_user
        to = [email]
        subject = 'Forgot password'
        random_number = random.randint(100000, 999999)
        body = f'Dear user, \nThank you for using Trave18tor!\nYour code to change password is: {random_number}\nWe kindly request you do not share this code.\n\nBest regards,\nTrave18tor team'
        email_text = """\
        From: %s
        To: %s
        Subject: %s

        %s
        """ % (sent_from, ", ".join(to), subject, body)

        try:
            smtp_server = smtplib.SMTP('smtp.gmail.com', 587)
            smtp_server.starttls()
            smtp_server.login(gmail_user, gmail_password)
            smtp_server.sendmail(sent_from, to, email_text)
            smtp_server.close()
            random_number = str(random_number)
            return random_number
        except Exception as ex:
            print("Something went wrong…", ex)

    def send_custom_email(self, email, subject, body):
        '''
        Constructs a custom email that takes in a subject title and a body to send to the provided email address.
        '''
        gmail_user = 'travel8tor23@gmail.com'
        gmail_password = 'xgmz rcrv qofn cwmj'

        sent_from = gmail_user
        to = [email]
        random_number = random.randint(100000, 999999)
        email_text = """\
        From: %s
        To: %s
        Subject: %s

        %s
        """ % (sent_from, ", ".join(to), subject, body)

        try:
            smtp_server = smtplib.SMTP('smtp.gmail.com', 587)
            smtp_server.starttls()
            smtp_server.login(gmail_user, gmail_password)
            smtp_server.sendmail(sent_from, to, email_text)
            smtp_server.close()
            random_number = str(random_number)
            return random_number
        except Exception as ex:
            print("Something went wrong…", ex)


    def save_new_password(self, email, password):
        '''
        Saves a new password for the user associated with the provided email address.
        '''
        data = AccountData()
        userId = data.save_new_password(email, password)
        return userId
